git submodule add https://github.com/vim-airline/vim-airline.git pack/myplugins/start/vim-airline
git submodule add https://github.com/vim-airline/vim-airline-themes.git pack/myplugins/start/vim-airline-themes
git submodule add https://github.com/vim-syntastic/syntastic.git pack/myplugins/start/syntastic
git submodule add https://github.com/gmoe/vim-espresso.git pack/myplugins/start/vim-espresso
git submodule add https://github.com/NLKNguyen/papercolor-theme.git pack/myplugins/start/papercolor-theme
git submodule add https://github.com/tpope/vim-fugitive.git pack/myplugins/start/vim-fugitive
git submodule add https://github.com/rust-lang/rust.vim.git pack/myplugins/start/rust.vim
git submodule add https://github.com/majutsushi/tagbar.git pack/myplugins/start/tagbar
git submodule add https://github.com/scrooloose/nerdcommenter.git pack/myplugins/start/nerdcommenter
git submodule add https://github.com/derekwyatt/vim-fswitch.git pack/myplugins/start/vim-fswitch
git submodule add https://github.com/ctrlpvim/ctrlp.vim.git pack/myplugins/start/ctrlp.vim

