## Add a new plugin that is autoloaded
   > git submodule add https://github.com/vim-airline/vim-airline.git pack/myplugins/start/vim-airline

## Add a new plugin that is manually loaded
   > git submodule add https://github.com/vim-airline/vim-airline.git pack/myplugins/opt/vim-airline

## Updating plugins
   > git submodule update --remote --merge<br>
   > git commit<br>

## Removing a plugin
   > git submodule deinit pack/myplugins/start/vim-airline<br>
   > git rm pack/myplugins/start/vim-airline<br>
   > rm -Rf pack/myplugins/start/vim-airline<br>
   > git commit<br>

## Regenerating the help tags after plugins added or changed
   > :helptags ALL

## Powerline fonts on Windows
- Download zip file from https://github.com/powerline/fonts
- In Admin Powershell
   > Set-ExecutionPolicy Bypass<br>
   > .\install.ps<br>
- Have to set the default font to 'Ubuntu Mono derivative for Powerline' for cmd.exe
