set columns=169
set lines=49
winpos 286 0
set winaltkeys=yes
set mouse=a

"set background=dark
"colorscheme darkblue
"colorscheme visualstudio
"colorscheme espresso

set guioptions-=T "get rid of toolbar
set guioptions-=t "get rid of tearoff menus
"set guioptions-=m "get rid of menu
"set guifont=-adobe-courier-medium-r-*-*-14-*-*-*-*-*-*-*
"syntax on


" Override any fonts set in the colorscheme
" set guifont=Consolas:h11:cANSI:qDRAFT
set guifont=Ubuntu_Mono_derivative_Powerlin:h11:cANSI:qDRAFT

