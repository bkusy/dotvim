set nocompatible
filetype plugin indent on

set backspace=indent,eol,start  " Allow backspacing over everything in insert mode.

set history=200		            " keep 200 lines of command line history
set ruler		                " show the cursor position all the time
set showcmd		                " display incomplete commands
set wildmenu		            " display completion matches in a status line

set ttimeout		            " time out for key codes
set ttimeoutlen=100	            " wait up to 100ms after Esc for special key

" Doesn't work in older vims (<8.0)
" set display=truncate            " Show @@@ in the last line if it is truncated.

" Show a few lines of context around the cursor.  Note that this makes the
" text scroll if you mouse-click near the start or end of the window.
set scrolloff=5

" Do incremental searching when it's possible to timeout.
if has('reltime')
	set incsearch
endif

" Do not recognize octal numbers for Ctrl-A and Ctrl-X, most users find it
" confusing.
set nrformats-=octal

" Don't use Ex mode, use Q for formatting.
" Revert with ":unmap Q".
map Q gq

" CTRL-U in insert mode deletes a lot.  Use CTRL-G u to first break undo,
" so that you can undo CTRL-U after inserting a line break.
" Revert with ":iunmap <C-U>".
inoremap <C-U> <C-G>u<C-U>

syntax on
let c_comment_strings=1

" Only do this part when compiled with support for autocommands.
if has("autocmd")
	augroup vimStartup
		au!
		" When editing a file, always jump to the last known cursor position.
		" Don't do it when the position is invalid, when inside an event handler
		" (happens when dropping a file on gvim) and for a commit message (it's
		" likely a different one than last time).
		autocmd BufReadPost *
			\ if line("'\"") >= 1 && line("'\"") <= line("$") && &ft !~# 'commit'
			\ | exe "normal! g`\""
			\ | endif
	augroup END
endif " has("autocmd")

" Convenient command to see the difference between the current buffer and the
" file it was loaded from, thus the changes you made.
" Only define it when not defined already.
" Revert with: ":delcommand DiffOrig".
if !exists(":DiffOrig")
	command DiffOrig vert new | set bt=nofile | r ++edit # | 0d_ | diffthis | wincmd p | diffthis
endif

if has('langmap') && exists('+langremap')
	" Prevent that the langmap option applies to characters that result from a
	" mapping.  If set (default), this may break plugins (but it's backward
	" compatible).
	set nolangremap
endif

set secure
set autoread
set autowrite
set tabstop=8
set shiftwidth=8
set nobackup
set nohlsearch

" We just want autoindent!
set autoindent
set cinoptions=g0
set nocindent

set smarttab
set showmatch
set noexpandtab
set shiftround
set nowarn
set noerrorbells
set novisualbell
set browsedir=buffer
set ch=2 "get rid of 'Hit enter to continue messages'
set laststatus=2 "always display a status line
set noundofile
set mouse=a
set clipboard=unnamed
set number
set encoding=utf-8
"set hidden


set t_Co=256
set t_vb= "Kill visual bell

if has("gui_running")
	set background=light
	" colorscheme PaperColor
	" let g:airline_theme='papercolor'
	colorscheme espresso
	let g:airline_theme='distinguished'
else
	" Tell Vim to erase areas of the screen as it does when
	" back-scrolling, using the current background color (from the VIM
	" color sceheme) instead of relying on the specific Terminal's
	" implenentation of Background Color Erase (BCE) mode.
	" This is necessary to get around issues with the WSL terminal on
	" Windows 10.
	set t_ut=""

	set background=dark
	colorscheme espresso
	let g:airline_theme='distinguished'
end



" TODO: Add test for platform and select correct line.
"set wildignore+=*/tmp/*,*.so,*.swp,*.zip " MacOSX/Linux
set wildignore+=*\\tmp\\*,*.swp,*.zip,*.exe " Windows

let mapleader = ","


"Clang-Format
" use .clang-format file
let g:clang_format#detect_style_file = 1
" format the current buffer on save.
let g:clang_format#auto_format = 1
" let g:clang_format#command = 'C:\\Program Files(x86)\\Microsoft Visual Studio\\2017\\Professional\\common7\\IDE\\VC\\vcpackages\\clang-format.exe'
map <C-R><C-D> :ClangFormat<CR>
imap <C-R><C-D> :ClangFormat<CR>

"Airline
let g:airline_powerline_fonts=1


let g:ctrlp_custom_ignore = {
	\ 'dir': '\v[\/]\.(git|hg|svn)$',
	\ 'file': '\v\.(exe|so|dll|o|d)$',
	\ 'link': 'SOME_BAD_SYMBOLIC_LINKS',
	\ }

let g:ctrlp_root_markers = ['meson.build']

"let g:ctrlp_user_command = 'find %s -type f'        " MacOSX/Linux
let g:ctrlp_user_command = 'dir %s /-n /b /s /a-d'  " Windows

"vim-localrc
let g:localrc_filename = 'vim.proj'

" NERD Commenter
let g:NERDDefaultAlign = 'left'
let g:NERDSpaceDelims = 1  	" Use '// foo' instead of '//foo'
let g:NERDAltDelims_c = 1  	" Use // instead of /* */

" Syntastic
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
" Ignore C/C++ files for now as things aren't configured properly yet.
let g:syntastic_ignore_files = ['\m\c.cpp$', '\m\c.c$', '\m\c.h$', '\m\c.hpp$']

" FSwitch
let g:fsnonewfiles = 1
autocmd! BufEnter *.c let b:fswitchdst = 'h,hpp' | let b:fswitchlocs = 'reg:/src/include'
autocmd! BufEnter *.cpp let b:fswitchdst = 'hpp,h' | let b:fswitchlocs = 'reg:/include/src'
map <Leader>oo :FSHere<CR>

autocmd BufNewFile,BufRead *.vcxproj,*.vcxproj.filters,*.sln setfiletype xml

"Emulate VisualStudio shift operations.
vmap <Tab> >gv
vmap <S-Tab> <gv

"Explore the directory of the current buffer.
let g:explSuffixesLast=0
map <Leader>f :e <C-R>=expand("%:p:h")<CR>
"map <C-`> :e <C-R>=expand("%:p:h")<CR>

"Explore the current list of buffers.
let g:bufExplorerSortBy='mru'
let g:bufExplorerShowDirectories=0
map <Leader>e :BufExplorer<CR>

"Check file out of perforce.
" map ,c :!p4 edit %

"Diff vs. head version in perforce.
" map ,d :!start p4 diff %

"Quick Advance in error buffer.
map <Leader>n :cn<CR>

"Kill macro shit
map q <Nop>


set makeprg=mbuild

:let g:netrw_sort_sequence='[\/]$,[^\/]$,'

